# 原神pck语音提取工具

origined from [https://github.com/vgmstream/vgmstream](https://github.com/vgmstream/vgmstream) and [https://github.com/Vextil/Wwise-Unpacker](https://github.com/Vextil/Wwise-Unpacker)

缝合了工具，原来的unpacker没法解密原神的pck，但是vgmstream可以，于是果断改脚本缝合

Unpack game audio Wwise files (pck, bnk)

解包pck和bnk文件

**This guide is for Windows-64bit only!**

put pck files in Game Files folder, then double click "Unpack to WAV"

把pck文件（找游戏安装目录）放到“Game Files”文件夹，然后双击脚本
Then the WAV file will be unpacked in WAV folder

然后等一会就可以了，提取出来的音频会在WAV文件夹里
